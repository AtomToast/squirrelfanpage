# Squirrelfanpage

An amazing place for fans of squirrels from all over the world to gather and celebrate the awesome nature of natures little rodents.  
Our goal is to connect people over their shared love and build a community.

This project runs on Django with a PostgreSQL database that we interface
through a RESTful API, managed by the Django ORM.

# Project setup

## Structures

### Structure of the project files

The project is placed inside of the `/squirrelfanbase` folder. Inside of this
folder we have our different Apps. This is following the general Django
convention. However it may be subject to changes and refactoring.

```
 ./
├──  squirrelfanbase/
│ ├──  account/
│ ├──  board/
│ ├──  media/
│ ├──  squirrelfanbase/
│ ├──  templates/
│ └──  manage.py*
├──  LICENSE
├──  README.md
└──  requirements.txt
```

The main part of our project is inside of the `squirrelfanbase/board`
directory.

```
 board/
├──  migrations/
│  ├──  0001_initial.py
│  └── ...
├──  templates/
│  ├──  board/
│  │  ├──  create_post.html
│  │  └── ...
│  └──  comment/
│     └── ...
├──  __init__.py
├──  admin.py
├──  apps.py
├──  forms.py
├──  models.py
├──  tests.py
├──  urls.py
└──  views.py
```

Overall it is a pretty standard Django setup.

### Structure of the git repository

Our repository consists of a `main` branch that can only be pushed to through
pull requests. This is done from the `development` branch, everytime we have a
working new stable update. The changes will be done in a squash merge,
while the detailed history persists on `development`
Changes are developed on individual, small feature branches. Once merged, those branches are deleted again.

### Structure of the database

![](er-diagram.png)

The database consists of three main tables: `Users`, `Posts` and `Comments`. There
are also 3 intermediate tables for our many-to-many relationships in
`CommentLikes`, `PostLikes` and `Followers`.
A User can both own and like posts and comments as well as follow other users.
Each comment has to belong to a post as well as optionally belong to an
existing tree of comments and be a reply to another user.  
A few notable things are that our Comments are stored in a tree, utilising
Modified Preorder Tree Traversal (MPTT). This is done to improve lookup speeds
in our database since we expect proportionally more people to read comments
than to actually post any.  
Also worth noting is that we use UUIDs as our ids for posts at the moment. This
has to do with an issue we had with naming the uploaded images, since a
sequential id would not be known at the point of creating the post. Creating a
UUID during creation solved this for now, however eventually we may want to
look to reiterate on this since working with UUIDs is a lot more costly.

## Set up development environment

### Set up a python venv

We set up the project with python version 3.9. So make sure you also use that.

```
python3.9 -m venv env
```

To enable the virtual environment run

```
source env/bin/activate
```

Then install dependencies:

All the dependencies are stored inside of the `requirements.txt`. Simply install
them with

```
pip install -Ir requirements.txt
```

once inside the virtual environment

### Set up PostgreSQL

First set up PostgreSQL on your system.

Then create a database for the project:

```
psql postgres -c "CREATE DATABASE squirreldb;"
```

Create a user for Django:

```
CREATE USER django WITH PASSWORD 'CP.1!gFx';
```

And grant them all the privileges on our new database:

```
GRANT ALL PRIVILEGES ON DATABASE squirreldb TO django;
```

Then all that is left is to populate your database with

```
./squirrelfanbase/manage.py migrate
```

### Run the project

To run the page for testing purposes, run the server like this:

```
./squirrelfanbase/manage.py runserver
```

### (Optional) Set up the pre-commit hooks

We recommend using a linter like `pyright` that includes static type checking.
However there is also a pre commit hook running `mypy` that you can set up
simply with (pre commit hooks are currently don't do much because we had some
issues)

```
pre-commit install
```

## Who worked on what

Overall we worked quite a lot in pair programming in video working sessions.
Still there was a general tendency that Alex worked on the account side of
things, while Kjell worked on the board with posts and likes. Both worked on
the comments: Alex implemented the base comment functionality, which Kjell then
extended with the replies.

Here is a Screenshot from our contributions.
![](contributions.png)

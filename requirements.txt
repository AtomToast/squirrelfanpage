autopep8==1.5.5
Django==3.1.7
pre-commit==2.10.1
psycopg2==2.8.6
pylint==2.7.2
django-stubs==1.7.0
Pillow==8.1.2
django-mptt==0.12.0

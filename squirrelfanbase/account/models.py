from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# FIXME: Cannot save image, may have different issues, with Bio and Status


def get_profile_image(self, filename):
    return f'profile_images/{self.pk}/{"profile_image.png"}'
# FIXME: Throws Error on GET - Doesn't have the right path


def get_default_profile_image():
    return "default/default.png"


class AccountManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError("Users need to provide an email.")
        if not username:
            raise ValueError("Users need to have a username.")
        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
            email=self.normalize_email(email),
            username=username,
            password=password
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser):
    # Custom Fields - FIXME: Bio and Status shouldn't be required on save
    email = models.EmailField(verbose_name='email',
                              max_length=100, unique=True)
    username = models.CharField(max_length=50, unique=True)
    description = models.CharField(verbose_name='bio', max_length=250)
    # FIXME: name this properly without the user_
    user_status = models.CharField(verbose_name='status', max_length=25)
    profile_image = models.ImageField(
        max_length=255, upload_to=get_profile_image, null=True, blank=True, default=get_default_profile_image)
    following = models.ManyToManyField('self', through='Followers',
                                       related_name='followers',
                                       related_query_name='follower',
                                       symmetrical=False)

    # Needed Fields
    # FIXME: this is not just the date
    date_joined = models.DateTimeField(
        verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    # Set custom manager to model
    objects = AccountManager()

    # Requirement Fields
    USERNAME_FIELD = 'email'  # login with Email - can also be username, if changed
    # Believe the username is already a required field so it doesn't have to be here
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

    # This is to fetch profile images uploaded to set their name to desired
    def get_profile_image_filename(self):
        return str(self.profile_image)[str(self.profile_image).index(f'profile_images/{self.pk}/'):]


class Followers(models.Model):
    user_following = models.ForeignKey(Account, on_delete=models.CASCADE,
                                       related_name='+')
    user_being_followed = models.ForeignKey(Account, on_delete=models.CASCADE,
                                            related_name='+')
    followed_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user_following',
                                            'user_being_followed'],
                                    name='unique_followers'),
            models.CheckConstraint(check=~models.Q(
                user_following=models.F("user_being_followed")),
                name='not_following_yourself')
        ]

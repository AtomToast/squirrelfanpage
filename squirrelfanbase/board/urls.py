from django.urls import include, path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('create', views.create_post, name='create_post'),
    path('<id>/', views.detail_view_post, name='detail_view_post'),
    path('<id>/update', views.update_view_post, name='update_view_post'),
    path('<id>/delete', views.delete_post, name='delete_post'),
    path('<post_id>/<comment_id>/reply',
         views.reply_comment, name='reply_comment'),
    path('<post_id>/<comment_id>/delete', views.delete_comment, name='delete_comment'),
]

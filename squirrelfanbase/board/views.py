from board.forms import CreatePostForm, UpdatePostForm, CreateCommentForm
from .models import Post, Comment
from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, HttpResponse


def index(request):
    latest_post_list = Post.objects.order_by('-created_at')[:5]
    return render(request, 'board/index.html', {'latest_post_list':
                                                latest_post_list})


def create_post(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(f"/board")

    form = CreatePostForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.save()
        form = CreatePostForm()
        return HttpResponseRedirect(f"/board/{obj.id}")

    return render(request, "board/create_post.html", {})


def detail_view_post(request, id):
    post = Post.objects.get(id=id)
    comments = post.comments.filter()

    user_comment = None

    if request.method == 'POST':
        if not request.user.is_authenticated:
            return HttpResponseRedirect(f"/board/{post.id}")

        comment_form = CreateCommentForm(request.POST or None, request.FILES or None)
        if comment_form.is_valid():
            user_comment = comment_form.save(commit=False)
            user_comment.post = post
            user_comment.author = request.user
            user_comment.save()
            return HttpResponseRedirect('/board/' + str(post.id))
    else:
        comment_form = CreateCommentForm()
    return render(request, "board/detail_view_post.html", {"post": post, "comments": user_comment, "comments": comments, "comment_form": comment_form, "user": request.user})


def update_view_post(request, id):
    obj = get_object_or_404(Post, id=id)

    if request.user != obj.author:
        return HttpResponse("You stink. That's not allowed")

    form = UpdatePostForm(request.POST or None, instance=obj)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect(f"/board/{id}")

    return render(request, "board/update_view_post.html", {"form": form,
                                                           "data": obj})


def delete_post(request, id):
    obj = get_object_or_404(Post, id=id)

    if request.user != obj.author:
        return HttpResponse("You stink. That's not allowed")

    if request.method == "POST":
        obj.delete()
        return HttpResponseRedirect("/board")

    return render(request, "board/delete_post.html", {})

def delete_comment(request, post_id, comment_id):
    obj = get_object_or_404(Comment, id=comment_id)
    if request.user != obj.author:
        return HttpResponse("You shouldn't be here. Leave!")

    if request.method == "POST":
        obj.delete()
        return HttpResponseRedirect(f"/board/{obj.post.id}")
    return render(request, "comment/delete_comment.html")



def reply_comment(request, post_id, comment_id):
    post = get_object_or_404(Post, id=post_id)
    parent_comment = get_object_or_404(Comment, id=comment_id)

    if request.method == "POST":
        comment_form = CreateCommentForm(request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.post = post
            new_comment.author = request.user
            new_comment.parent_id = parent_comment.get_root().id
            new_comment.reply_to = parent_comment.author

            new_comment.save()
            return HttpResponseRedirect(f"/board/{post.id}/#comment-{new_comment.id}")
        else:
            return HttpResponse("The form is incorrect, please fill it in again. ")

    elif request.method == "GET":
        comment_form = CreateCommentForm()
        context = {
            'comment_form': comment_form,
            'post': post,
            'parent_comment': parent_comment
        }
        return render(request, 'comment/reply_comment.html', context)
    else:
        return HttpResponse("Only GET/POST requests are accepted. ")

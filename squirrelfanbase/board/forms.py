from django import forms
from board.models import Post, Comment
from mptt.forms import TreeNodeChoiceField

class CreatePostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'description', 'image']


class UpdatePostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'description']


class CreateCommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['body']

from django.contrib import admin

from .models import Post, Comment


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'image',)
    exclude = ('slug', 'author')


class CommentAdmin(admin.ModelAdmin):
    list_display = ('body', 'author', "parent")


admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)

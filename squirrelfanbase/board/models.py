import datetime
import uuid
from django.db import models
from django.utils import timezone
from PIL import Image
from django.db.models.signals import pre_save, post_delete
from django.utils.text import slugify
from django.conf import settings
from urllib.parse import urlparse
from django.dispatch import receiver
from mptt.models import MPTTModel, TreeForeignKey


def get_thumb_image(instance, filename):
    file_path = 'post/{instance_id}/{title}-{filename}'.format(
        instance_id=str(instance.id), title=str(instance.title), filename=filename
    )
    return file_path


class Post(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=300, blank=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1, blank=False)
    image = models.ImageField(upload_to=get_thumb_image, height_field=None, width_field=None,
                              max_length=100, default=urlparse('i.stack.imgur.com/y9DpT.jpg'))
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited_at = models.DateTimeField(auto_now=True)
    slug = models.SlugField(blank=True, unique=True)
    likes = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                   through='PostLikes',
                                   related_name='post_likes',
                                   related_query_name='post_like')

    def __str__(self):
        return self.title

    def was_submitted_recently(self):
        return self.created_at >= timezone.now() - datetime.timedelta(days=1)


class PostLikes(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    post = models.ForeignKey(Post,
                             on_delete=models.CASCADE)
    liked_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user',
                                            'post'],
                                    name='unique_post_likes'),
        ]


@receiver(post_delete, sender=Post)
def submission_delete(sender, instance, **kwargs):
    instance.image.delete(False)


def pre_save_post_receiver(sender, instance, **kwags):
    if not instance.slug:
        instance.slug = slugify(
            instance.author.username + "-" + instance.title)


pre_save.connect(pre_save_post_receiver, sender=Post)


class Comment(MPTTModel):
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1,
        blank=False, related_name="comments")
    parent = TreeForeignKey(
        'self', null=True, blank=True, on_delete=models.CASCADE,
        related_name='children')
    reply_to = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='replyers'
    )
    post = models.ForeignKey(Post, on_delete=models.CASCADE,
                             related_name="comments")
    body = models.TextField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    likes = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                   through='CommentLikes',
                                   related_name='comment_likes',
                                   related_query_name='comment_like')

    class MPTTMeta:
        order_insertion_by = ['created_at']

    def __str__(self) -> str:
        return "<Comment: {} {}>".format(self.author.username,
                                         self.created_at.isoformat())

    def __repr__(self) -> str:
        return self.__str__()


class CommentLikes(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    comment = models.ForeignKey(Comment,
                                on_delete=models.CASCADE)
    liked_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user',
                                            'comment'],
                                    name='unique_comment_likes'),
        ]

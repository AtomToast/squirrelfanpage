# Generated by Django 3.1.7 on 2021-03-22 12:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('board', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='post_created',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='post',
            old_name='post_last_edited',
            new_name='last_edited_at',
        ),
        migrations.RemoveField(
            model_name='post',
            name='desc',
        ),
        migrations.AddField(
            model_name='post',
            name='description',
            field=models.TextField(blank=True, max_length=300),
        ),
    ]
